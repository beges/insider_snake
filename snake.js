//Barış Ege Sevgili

(function(){
	jQuery(document).off();			// Inorder to handle the collision with the webpage's query
	var snakeBody = jQuery('<div class="snakeBody">');
	var score= jQuery('<div class="score">0</div>');
	var food= jQuery('<div class="food">');
	var isEnded=false;				//boolean for detecting whether game is ended
	var result=0;					//current score, updated at every change
	var snake_length=7;				//length of the snake set to 7 initially
	//var isSaved=false;
	
	var positionList = JSON.parse(localStorage.getItem('positionList') || "[]");

	var direction = JSON.parse(localStorage.getItem('direction')) || {
		horizontal: 1,
		vertical: 0
	};

	var getNextPosition = function(elem){
		var position = {};
        //if next move collides with any of the borders, game ends
		var horizontalBorder = Number(jQuery(elem).css('left').replace('px','')) + (direction.horizontal * 15);
        var verticalBorder = Number(jQuery(elem).css('top').replace('px','')) + (direction.vertical * 15);
        //checks the collisions with the borders of the screen and the snake, ends the game if collision exists
        if(horizontalBorder<0 || horizontalBorder> jQuery(window).width() ||  verticalBorder<0 || verticalBorder>jQuery(window).height()){
            
                isEnded=true;
                alert("Gameover, your score is: "+ result + "!!");
        }   
       
		
		
		//***
		
		
		direction.horizontal && (position.left = Number(jQuery(elem).css('left').replace('px','')) + (direction.horizontal * 15));
		direction.vertical && (position.top = Number(jQuery(elem).css('top').replace('px','')) + (direction.vertical * 15));

		position.top && ((position.top > jQuery(window).height() && (position.top = 0)) || (position.top < 0 && (position.top = jQuery(window).height())));
		position.left && ((position.left > jQuery(window).width() && (position.left = 0)) || (position.left < 0 && (position.left = jQuery(window).width())));

		return position;
	};
	
	//creates the food at a random location within the screen
	var foodPos = function(){
        var x = (Math.round(Math.random() * jQuery(window).width()));
        var y = (Math.round(Math.random() * jQuery(window).height()));
        x = x-x%15;			//to fit the food in the pixel size we use
        y = y-y%15;
		
		var foodLocation={	//to use in save/load operation
			left:x,
			top:y
		};
        return {
            left:x,
            top:y
        }
    }
	// a function for save operation
	var getElement = function(){
		var targetArea = {
			top: Number(jQuery('.snakeBody:first').css('top').replace('px','')) + (direction.vertical && direction.vertical * 15),
			left: Number(jQuery('.snakeBody:first').css('left').replace('px','')) + (direction.horizontal && direction.horizontal * 15)
		};
		return jQuery(document.elementFromPoint(targetArea.left, targetArea.top));
	};

	var saveGameData = function(){
		localStorage.setItem('positionList', JSON.stringify(positionList));
		localStorage.setItem('direction', JSON.stringify(direction));
		//localStorage.setItem('foodLocation', JSON.stringify(foodLocation));
	};
	
	
	/*handles the result of each keys of the game: 
			keycodes
	up:		38
	down:	40
	right:	39
	left:	37
	space: 	32
	
	
	
	
	*/
	jQuery(document).keydown(function(e){

		if(jQuery.inArray(e.keyCode , [32,37,38,39,40]) == -1) return;	//if some other key is pressed

		/*
		horizontal: -1 means snake moves through left in horizon, +1 means right
		vertical: -1 means snake moves upward in vertical, -1 means downward
		0 means no change in that property (vertical: 0 means snake's vertical location is not changing)
		*/
		
		
		
		if(e.keyCode == 32){		//space key is provided for saving the game
			saveGameData();
			//isSaved=true;
			getElement().length >0 && (getElement()[0].click());
			e.preventDefault();
			return;
			
			//inner if statements prevents the snake from going the way its coming from
		}else if(e.keyCode==37) {	//left arrow makes the snake turn left :) 
            if (direction.horizontal != 1) {
                direction = {
                    vertical: 0,
                    horizontal: -1
                }
            }
        }else if(e.keyCode==38) {	//up arrow makes the snake go up
            if (direction.vertical != 1) {
                direction = {
                    vertical: -1,
                    horizontal: 0
                }
            }
        }else if(e.keyCode==39) {	//right arrow makes the snake turn right
            if (direction.horizontal != -1) {
                direction = {
                    vertical: 0,
                    horizontal: 1
                }
            }
        }else if(e.keyCode==40){	//down arrow makes the snake go down
            if(direction.vertical!=-1){
                direction = {
                    vertical: 1,
                    horizontal: 0
                }
            }
        }

		e.preventDefault();

	});

	//function that updates the value in the scoreboard
	function scoreBoard(){
        score.text("Score: "+result);
    }
	
	//function for incrementing the result by one at each 10 seconds
	setInterval (function timeUpdate(){
        if(isEnded)
            return;
        result+=1;
        scoreBoard();

    },10000);
	
	
	setInterval(function(){
		if(isEnded)
			return;
		
		jQuery('.snakeBody').each(function(key){
			positionList[key] = {
				top: Number(jQuery(this).css('top').replace('px','')),
				left: Number(jQuery(this).css('left').replace('px',''))
			};
			if(key == 0)
				jQuery(this).css(getNextPosition(jQuery(this)));
			else 
				jQuery(this).css(positionList[key-1]);			

		});
		
		//ends the game if snake eats itself
		for(var i=0; i<positionList.length; i++){
			
			if((jQuery('.snakeBody:first').css('left').replace('px','') == positionList[i]['left'])&&(jQuery('.snakeBody:first').css('top').replace('px','') == positionList[i]['top'])){
				isEnded=true;
				alert("Gameover, your score is: "+ result + "!!");
			}
			
		}
		//increments the result by 2 if snake eats the food, after eating creates a new food by calling foodPos function
		if(jQuery('.snakeBody:first').css('top') == jQuery('.food').css('top') && jQuery('.snakeBody:first').css('left') == jQuery('.food').css('left')){
            jQuery('.food').css(foodPos());
            result+=2;
            snake_length+=1;
            jQuery('body').append(snakeBody.clone().css(positionList[snake_length-1] || {}));
            scoreBoard();

        }

	}, 200); //200 denotes the speed of the snake



	jQuery('body').append('<style>.snakeBody { z-index:99999999; width: 15px; height: 15px; position: fixed; top: 0; left:0; background: black;} .snakeBody:first-child {background: dodgerblue;}</style>');
	jQuery('body').append('<style>.food{ z-index:99999999; width: 15px; height: 15px; position: fixed; top:0; left:0; background: lightpink;}</style>');
	jQuery('body').append('<style>.score{ z-index:9999999; width: 60px; height: 30px; position: fixed; top:0; left:0; background: aqua;}</style>');
	
	//creates the snake at each iteration, with its current length and position, by positionList
	var i=0;
	while(i<snake_length){
		jQuery('body').append(snakeBody.clone().css(positionList[i] || {}));
		i++;
	}
	jQuery('.snakeBody:first').css('background','dodgerblue');
	jQuery('body').append(score);
	scoreBoard();	
	jQuery('body').append(food.css(foodPos()));

})();